//
//  main.m
//  edm_disabler
//
//  Created by bocal on 20/07/2020.
//  Copyright © 2020 EDM42. All rights reserved.
//

#import <Cocoa/Cocoa.h>

extern CGError CGSConfigureDisplayEnabled(CGDisplayConfigRef, CGDirectDisplayID, bool);
extern CGDisplayErr CGSGetDisplayList(CGDisplayCount maxDisplays, CGDirectDisplayID * onlineDspys, CGDisplayCount * dspyCnt);

@implementation NSString (ShellExecution)

- (NSString*)runAsCommand {
    NSPipe* pipe = [NSPipe pipe];

    NSTask* task = [[NSTask alloc] init];
    [task setLaunchPath: @"/bin/sh"];
    [task setArguments:@[@"-c", [NSString stringWithFormat:@"%@", self]]];
    [task setStandardOutput:pipe];

    NSFileHandle* file = [pipe fileHandleForReading];
    [task launch];

    return [[NSString alloc] initWithData:[file readDataToEndOfFile] encoding:NSUTF8StringEncoding];
}

@end

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        @try {
            CGDisplayConfigRef config;
            CGDisplayCount nDisplays = 0;
            CGDirectDisplayID displayList[0x10];
            NSString *output = NULL;

            NSLog(@"Launching EDM_DISABLER...");
            NSLog(@"Get display list...");
            CGSGetDisplayList(0x10, displayList, &nDisplays);
            NSLog(@"Begin display configuration...");
            CGBeginDisplayConfiguration(&config);
            NSLog(@"Get entry...");
            io_registry_entry_t entry = IORegistryEntryFromPath(kIOMasterPortDefault, "IOService:/IOResources/IODisplayWrangler");
            if (entry) {
                if (displayList[0]) {
                    NSLog(@"%i", displayList[0]);
                } else {
                    NSLog(@"No display found... exiting with error !");
                    return (1);
                }
                NSLog(@"Get ip...");
                while(output == NULL || [output length] == 0) {
                    output = [@"ifconfig en0 |awk '/inet / {print $2; }'" runAsCommand];
                }
                NSLog(@"Processing last char..");
                NSString *lastChar = [output substringFromIndex:[output length] - 2];
                NSLog(@"%@", output);
                if ([lastChar intValue] % 2) {
                    NSLog(@"Configure display fade effect...");
                    CGConfigureDisplayFadeEffect(config, 0, 0, 0, 0, 0);
                    NSLog(@"Set CF property at true...");
                    IORegistryEntrySetCFProperty(entry, CFSTR("IORequestIdle"), kCFBooleanTrue);
                    //NSLog(@"Sleeping..");
                    usleep(100 * 10); // sleep 100 ms
                    //NSLog(@"Set CF property at false...");
                    IORegistryEntrySetCFProperty(entry, CFSTR("IORequestIdle"), kCFBooleanFalse);
                    NSLog(@"Release entry...");
                    IOObjectRelease(entry);
                    NSLog(@"Disable display...");
                    CGSConfigureDisplayEnabled(config, displayList[0], false);
                    NSLog(@"Apply display configuration...");
                    CGCompleteDisplayConfiguration(config, kCGConfigurePermanently);
                    NSLog(@"SUCCESS !");
                } else {
                    NSLog(@"Not a mac we want to reboot... !");
                }
            }
            else {
                NSLog(@"No entry found !");
                return (1);
            }
        }
        @catch (NSException *exception) {
            NSLog(@"%@", exception.reason);
        }
    }
    return (0);
}